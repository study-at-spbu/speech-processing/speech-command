import json
from pathlib import Path

import numpy as np
import torch
import torch.nn.functional as F
import torch.optim as optim
import torchaudio
from matplotlib import pyplot as plt
from torch import Tensor
from torch.utils.data import DataLoader
from torch.utils.data import Dataset
from tqdm import tqdm

from model import Model

COMMAND: list[str] = ['Вверх', 'Влево', 'Вправо', 'Вниз']
BATCH_SIZE = 10
EPOCH_COUNT = 80
DATASET_PATH: Path = Path('Команды2')

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class AudioInfo:
    command_index: int
    path: Path
    source: Tensor
    rate: int

    def __init__(self, info: dict):
        self.command_index = COMMAND.index(info['command'])
        self.path = Path(info['path'])
        source, self.rate = torchaudio.load(str(self.path.resolve()))
        transform = torchaudio.transforms.Resample(orig_freq=self.rate, new_freq=8000)
        source = torch.mean(source, dim=0, keepdim=True)
        source = transform(source)
        self.source = source

    def getData(self):
        target = np.zeros(4, dtype=int)
        target[self.command_index] = 1
        return self.source, self.command_index


class CustomDataset(Dataset):
    _audio_infos: list[AudioInfo]

    def __init__(self, path: str):
        with open(path, mode='r', encoding='utf8') as file:
            dataset_infos = json.load(file)['data']

        self._audio_infos: list[AudioInfo] = [AudioInfo(dataset_info) for dataset_info in dataset_infos]

        super().__init__()

    def __len__(self):
        return len(self._audio_infos)

    def __getitem__(self, idx: int) -> AudioInfo:
        return self._audio_infos[idx]


def pad_sequence(batch):
    batch = [item.t() for item in batch]
    batch = torch.nn.utils.rnn.pad_sequence(batch, batch_first=True, padding_value=0.)
    return batch.permute(0, 2, 1)


def collate_fn(batch):
    tensors, targets = [], []

    for audio in batch:
        waveform, label = audio.getData()
        tensors += [waveform]
        targets += [torch.tensor(label)]

    tensors = pad_sequence(tensors)
    targets = torch.stack(targets)

    return tensors, targets


def train(model):
    model.train()
    losses_bach = []
    for data, target in train_loader:
        data = data.to(device)
        target = target.to(device)
        output = model(data)
        loss = F.nll_loss(output.squeeze(), target)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        pbar.update(pbar_update)
        losses_bach.append(loss.item())
    losses_epoch_train.append(np.mean(losses_bach))


def number_of_correct(pred, target):
    return pred.squeeze().eq(target).sum().item()


def get_likely_index(tensor):
    return tensor.argmax(dim=-1)


def test(model, epoch):
    model.eval()
    correct = 0
    losses_bach = []
    for data, target in test_loader:
        data = data.to(device)
        target = target.to(device)
        output = model(data)

        pred = get_likely_index(output)
        correct += number_of_correct(pred, target)
        loss = F.nll_loss(output.squeeze(), target)

        pbar.update(pbar_update)
        losses_bach.append(loss.item())
    losses_epoch_test.append(np.mean(losses_bach))

    print(f"Test Epoch: {epoch}\tAccuracy: {correct}/{len(test_set)} ({100. * correct / len(test_set):.0f}%) Loss: {loss}\n")
    accuracy_epoch_test.append(100. * correct / len(test_set))


def predict(tensor):
    tensor = tensor.to(device)
    tensor = model(tensor.unsqueeze(0))
    tensor = get_likely_index(tensor)
    return tensor.squeeze()


train_set = CustomDataset(str((DATASET_PATH / "train.json").resolve()))
test_set = CustomDataset(str((DATASET_PATH / "test.json").resolve()))
train_loader = DataLoader(
    train_set,
    batch_size=BATCH_SIZE,
    shuffle=True,
    collate_fn=collate_fn,
)

test_loader = DataLoader(
    test_set,
    batch_size=BATCH_SIZE,
    collate_fn=collate_fn,
)

model = Model(n_output=len(COMMAND))
model.to(device)

optimizer = optim.Adam(model.parameters(), lr=0.01, weight_decay=0.0001)
scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=20, gamma=0.1)

pbar_update = 1 / (len(train_loader) + len(test_loader))
losses_epoch_train = []
losses_epoch_test = []
accuracy_epoch_test = []

with tqdm(total=EPOCH_COUNT) as pbar:
    for epoch in range(1, EPOCH_COUNT + 1):
        train(model)
        test(model, epoch)
        scheduler.step()

for audio in test_set:
    waveform, label_index = audio.getData()
    output = predict(waveform)
    if output != label_index:
        print(f"Ошибка: {audio.path}. Ожидалось: {COMMAND[label_index]}. Получение: {COMMAND[output]}.")
        break
else:
    print("Идеальное выполнение")

fig, axs = plt.subplots(1, 2, figsize=(10, 5))

axs[0].plot(list(range(1, EPOCH_COUNT + 1)), losses_epoch_train, label="TrainLoss")
axs[0].plot(list(range(1, EPOCH_COUNT + 1)), losses_epoch_test, label="TestLoss")
axs[0].set_title('Loss')
axs[0].legend()

axs[1].plot(list(range(1, EPOCH_COUNT + 1)), accuracy_epoch_test, label="AccuracyTest")
axs[1].set_title('Accuracy')
axs[1].legend()

plt.show()

torch.save(model.state_dict(), "state.pt")
