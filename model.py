import torch.nn as nn
import torch.nn.functional as F


class Layer(nn.Module):
    def __init__(self, n_input, n_channel, kernel_size, stride=1) -> None:
        super().__init__()

        self._sequential = nn.Sequential(
            nn.Conv1d(n_input, n_channel, kernel_size=kernel_size, stride=stride),
            nn.BatchNorm1d(n_channel),
            nn.ReLU(),
            nn.MaxPool1d(4)
        )

    def forward(self, x):
        return self._sequential(x)


class Model(nn.Module):
    def __init__(self, n_input=1, n_output=4, n_channel=32):
        super().__init__()

        self._sequential = nn.Sequential(
            Layer(n_input, n_channel, 80, 3),
            Layer(n_channel, n_channel, 3),
            Layer(n_channel, 2 * n_channel, 3),
            Layer(2 * n_channel, 2 * n_channel, 3)
        )

        self.linear = nn.Linear(2 * n_channel, n_output)

    def forward(self, x):
        x = self._sequential(x)
        x = F.avg_pool1d(x, x.shape[-1])
        x = x.permute(0, 2, 1)
        x = self.linear(x)
        return F.softmax(x, dim=2)
