import shutil
import time
from pathlib import Path

import gymnasium as gym
import torch
import torchaudio
from gymnasium.envs.toy_text.frozen_lake import UP, LEFT, RIGHT, DOWN

from model import Model

SPEECH_AUDIO_FILE: Path = Path('Recording.wav')
RESULT_DIRECTORY: Path = Path("Result")

shutil.rmtree(RESULT_DIRECTORY)
RESULT_DIRECTORY.mkdir(parents=True, exist_ok=True)

SIZE = 7856
STEP = SIZE // 2

COMMAND: list[str] = ['Вверх', 'Влево', 'Вправо', 'Вниз']

map = [
    "FFFFFFFF",
    "FFFFFFFF",
    "FFFFFFFF",
    "FFFSFFFF",
    "FFFFFFFF",
    "FFFFFFFF",
    "FFFFFFFF",
    "FFFFFFFF",
]

GYM_COMMAND = [UP, LEFT, RIGHT, DOWN]

model = Model()
model.load_state_dict(torch.load("state.pt"))
model.eval()

source, rate = torchaudio.load(str(SPEECH_AUDIO_FILE.resolve()))
transform = torchaudio.transforms.Resample(orig_freq=rate, new_freq=8000)
source = torch.mean(source, dim=0, keepdim=True)
source = transform(source)

start = 0
save_index = 1
command_arr = []
while start <= source.shape[1] - SIZE:
    local_source = source[:, start:start + SIZE]
    tensor = local_source.to('cpu')
    result = model(tensor.unsqueeze(0))
    command_index = int(torch.argmax(result))
    if result[0][0][command_index] < 0.80:
        start += STEP
        continue

    word_command = COMMAND[command_index]
    print(word_command)
    start = start + SIZE + SIZE
    torchaudio.save(RESULT_DIRECTORY / f"{save_index}_{word_command}.wav", local_source, 8000)
    save_index += 1

    command_arr.append(command_index)

env = gym.make(("FrozenLake-v1"), render_mode="human", desc=map, is_slippery=False)
observation, info = env.reset()
time.sleep(0.8)

for command_index in command_arr:
    action = env.action_space.sample()
    observation, reward, terminated, truncated, info = env.step(GYM_COMMAND[command_index])

    time.sleep(0.8)

env.close()
